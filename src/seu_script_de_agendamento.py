import schedule
import time
import requests

def executar_analise():
    #response = requests.get('http://seu-app-fastapi-service:80/executar-analise')
    response = requests.get('http://0.0.0.0:80/executar-analise')
    print(response.json())

# Defina o cronograma para executar a análise a cada semana, às segundas-feiras às 9h
#schedule.every().monday.at("09:00").do(executar_analise)

# Defina o cronograma para executar a análise a cada 5 minutos
schedule.every(5).minutes.do(executar_analise)

# Loop para manter o script em execução
while True:
    schedule.run_pending()
    time.sleep(1)
