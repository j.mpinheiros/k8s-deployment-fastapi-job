from fastapi import FastAPI
import subprocess

app = FastAPI()

@app.get("/executar-analise")
def executar_analise():
    # Chama o script Python responsável pela análise
    subprocess.run(["python3", "seu_script.py"])
    return {"message": "Análise executada com sucesso"}
