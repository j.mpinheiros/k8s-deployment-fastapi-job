from datetime import datetime
import schedule
import time
import json
import os

# Get the directory of the current script
script_dir = os.path.dirname(os.path.realpath(__file__))

# Define the paths for log.log and report.json files
log_path = os.path.join(script_dir, "log.log")
report_path = os.path.join(script_dir, "report.json")

# Get current date and time
now = datetime.now()
current_time = now.strftime("%Y-%m-%d %H:%M:%S")

def process_data():
    # Process data and save to JSON file
    data = {
        "info01": "value01",
        "info02": "value02",
    }

    with open(report_path, 'w') as f:
        json.dump(data, f)
        print("Report Saved Successfully !")
    
    # Log the event
    with open(log_path, "a") as f:
        f.write(f"{current_time} - Report Saved Successfully...\n")

def send_report():
    # Send report and log the event
    print("Report Sent Successfully !")
    with open(log_path, "a") as f:
        f.write(f"{current_time} - Report Sent Successfully...\n")

def job():
    # Call functions to process data and send report
    print("Executing the Python script...")
    process_data()
    send_report()

    # Log the event
    with open(log_path, "a") as f:
        f.write(f"{current_time} - Executing the Python script...\n")


if __name__ == '__main__':
    time.sleep(10)
    job()
    time.sleep(5)
    print("Python script Executed...")