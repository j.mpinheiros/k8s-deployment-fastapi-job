# Use a imagem base Python 3.9
FROM python:3.9-slim

# Copia os arquivos necessários (seu aplicativo FastAPI e script de agendamento) para o contêiner
# COPY seu_app_fastapi.py /app/seu_app_fastapi.py
# COPY seu_script_de_agendamento.py /app/seu_script_de_agendamento.py
# COPY entrypoint.sh /app/entrypoint.sh

# Copie o diretório src para o contêiner
COPY src /app/
# Copie o arquivo de entrada entrypoint.sh para o contêiner
COPY entrypoint.sh /app/entrypoint.sh

# Defina o diretório de trabalho como /app
WORKDIR /app

# Instale as dependências do FastAPI
RUN pip install fastapi uvicorn schedule requests

# Torne o arquivo de entrada executável
RUN chmod +x entrypoint.sh

# Defina o comando de entrada para executar o arquivo de entrada
ENTRYPOINT ["./entrypoint.sh"]