#!/bin/bash

# chmod +x entrypoint.sh
# chmod -R 777 /app/

# Inicia o serviço FastAPI em segundo plano
uvicorn seu_app_fastapi:app --host 0.0.0.0 --port 80 &

# Aguarda 10 segundos para garantir que o serviço esteja pronto antes de executar o script
sleep 10

# Executa o script de agendamento
python3 seu_script_de_agendamento.py
