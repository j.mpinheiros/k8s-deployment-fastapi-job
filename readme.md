# ssh into k8s
    vi ~/cleanup-cronjob.yml

    kubectl apply -f ~/test-cronjob.yml
    kubectl get cronjob test-cronjob
    kubectl delete cronjob test-cronjob

# montar a img docker e enviar para o repositorio
    docker build -t jmpinheiro/run-py-script:2.0 .
    docker push jmpinheiro/run-py-script:2.0

# aplicar um deployment
    kubectl apply -f my-script-deployment.yml

# operações com pods
    kubectl exec -it podname -- bash
    kubectl logs nome-do-pod
    kubectl cp NAMESPACE/POD_NAME:/app ~/home
    kubectl exec -it test-cronjob-1709230620-v2qtl -- ls -lha /app
